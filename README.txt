AUTHOR
-------

 * Chris Coppenbarger ("ccoppen", http://drupal.org/user/595194)
 

Required Modules
------------

Chart API (Version used: 7.x-1.x-dev) - http://drupal.org/project/chart
Webform (Version used: 7.x-3.13) - http://drupal.org/project/webform
 